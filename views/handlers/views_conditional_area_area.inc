<?php

/**
 * @file
 *  Defines the views_conditional_area_area handler.
 */

class views_conditional_area_text extends views_handler_area {

  /**
   * Returns an array containing all operators available for selection within
   * the form. These options can depend on the selected filter handler.
   *
   * @param $handler
   *  The selected exposed filter handler.
   *
   * @return array
   *  An array containing all operators and associated metadata.
   */
  public function operators($handler) {
    $operators = array(
      '=' => array(
        'title' => t('Is equal to'),
        'form' => 'equals',
        'validate' => 'equals',
      ),
      '!=' => array(
        'title' => t('Is not equal to'),
        'form' => 'equals',
        'validate' => 'notEquals',
      ),
      'contains' => array(
        'title' => t('Contains'),
        'form' => 'simpleText',
        'validate' => 'contains',
      ),
      'notcontains' => array(
        'title' => t('Does not contain'),
        'form' => 'simpleText',
        'validate' => 'notContains',
      ),
    );

    $handler_operators = $handler->operator_options();
    // Add the "in" and "not in" operators if the handler allows them.
    if (isset($handler_operators['in'])) {
      $operators['in'] = array(
        'title' => t('Is one of'),
        'form' => 'in',
        'validate' => 'in',
      );
    }
    if (isset($handler_operators['in'])) {
      $operators['not in'] = array(
        'title' => t('Is not one of'),
        'form' => 'in',
        'validate' => 'notIn',
      );
    }

    // if the definition allows for the empty operator, add it.
    if (!empty($handler->definition['allow empty'])) {
      $operators += array(
        'empty' => array(
          'title' => t('Is empty (NULL)'),
          'validate' => 'empty',
        ),
        'not empty' => array(
          'title' => t('Is not empty (NOT NULL)'),
          'validate' => 'notEmpty',
        ),
      );
    }

    return $operators;
  }

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['if'] = array('default' => '');
    $options['condition'] = array('default' => '');
    $options['checkbox_is'] = array('default' => array());
    $options['is'] = array('default' => '');
    $options['then'] = array('default' => '', 'translatable' => TRUE, 'format_key' => 'format');
    $options['then_format'] = array('default' => NULL);
    $options['else'] = array('default' => '', 'translatable' => TRUE, 'format_key' => 'format');
    $options['else_format'] = array('default' => NULL);
    $options['tokenize'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form_options = !empty($form_state['values']['options']) ? $form_state['values']['options'] : $this->options;

    // Build an array of fields.
    $filter_options = array();
    // Get all filters and save them for use later.
    $filters = $this->getFilters();
    foreach ($filters as $filter => $handler) {
      // Only allow filters with single values.
      if ($handler->is_exposed() && empty($handler->options['expose']['multiple'])) {
        $filter_options[$filter] = $handler->ui_name();
      }
    }

    $form['if'] = array(
      '#type' => 'select',
      '#title' => t('Filter'),
      '#description' => t('Select the exposed filter you wish to run the condition on. <strong>Note: This only allows selecting of single value exposed filters.</strong>'),
      '#options' => $filter_options,
      '#default_value' => $this->options['if'],
      '#empty_option' => t('- Select filter -'),
      '#ajax' => array(
        'path' => views_ui_build_form_url($form_state),
      ),
      '#submit' => array('views_ui_config_item_form_submit_temporary'),
      '#executes_submit_callback' => TRUE,
    );

    $form['condition'] = array(
      '#type' => 'select',
      '#title' => t('Condition'),
      '#description' => t('Select the condition you wish to evaluate against.'),
      '#options' => array(),
      '#empty_option' => t('- Select condition -'),
      // Because we are updating the options dynamically this removes the error
      // saying an illegal choice has been detected.
      '#validated' => TRUE,
      '#default_value' => $this->options['condition'],
      '#ajax' => array(
        'path' => views_ui_build_form_url($form_state),
      ),
      '#submit' => array('views_ui_config_item_form_submit_temporary'),
      '#executes_submit_callback' => TRUE,
    );

    // Build the form elements which are dependent on the selected exposed filter.
    $this->buildConditionalElements($form, $form_state, $form_options);
  }

  /**
   * Builds the conditional form element based on either the ajax submitted
   * values or the stored views options.
   *
   * The ajax values take priority.
   *
   * @param array $form
   *  The form array to add the form elements passed by reference.
   * @param $form_state
   *  The form state for the given form.
   * @param $options
   *  The form options used for the conditions. This is wither form_state values
   *  if using ajax or the stored views options.
   */
  private function buildConditionalElements(&$form, $form_state, $options) {
    $handler = !empty($options['if']) ? $this->getFilters($options['if']) : '';

    // Create the form element for the condition field.
    if (!empty($options['if'])) {
      $operators = array();
      foreach ($this->operators($handler) as $key => $operator) {
        $operators[$key] = $operator['title'];
      }
      $form['condition']['#options'] = $operators;
    }

    // Create the form element for the condition value field. This changes
    // depending on the type of condition.
    if (!empty($options['if']) && !empty($options['condition'])) {
      $operators = $this->operators($handler);
      if (isset($operators[$options['condition']]['form'])) {
        $callback = $operators[$options['condition']]['form'] . 'FormElement';
        $this->{$callback}($form, $form_state, $handler);
      }
      else {
        $form['is'] = array(
          '#type' => 'item',
          '#title' => t('Conditional value'),
          '#markup' => t('This operator is either not available or does not allow the input of a value.'),
        );
      }

      // Adds in the text areas and tokens.
      $form['then'] = array(
        '#type' => 'text_format',
        '#title' => t('Then'),
        '#default_value' => $this->options['then'],
        '#rows' => 6,
        '#format' => isset($this->options['then_format']) ? $this->options['then_format'] : filter_default_format(),
        '#wysiwyg' => FALSE,
        '#description' => t('Enter the text you wish to display if the condition evaluates to TRUE.'),
      );

      $form['else'] = array(
        '#type' => 'text_format',
        '#title' => t('Else'),
        '#default_value' => $this->options['else'],
        '#rows' => 6,
        '#format' => isset($this->options['else_format']) ? $this->options['else_format'] : filter_default_format(),
        '#wysiwyg' => FALSE,
        '#description' => t('Enter the text you wish to display if the condition evaluates to FALSE.'),
      );

      // This is taken directly from the views_handler_area_text options form.
      $form['tokenize'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use replacement tokens from the first row'),
        '#default_value' => $this->options['tokenize'],
      );

      // Get a list of the available fields and arguments for token replacement.
      $options = array();
      foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
        $options[t('Fields')]["[$field]"] = $handler->ui_name();
      }

      $count = 0; // This lets us prepare the key as we want it printed.
      foreach ($this->view->display_handler->get_handlers('argument') as $arg => $handler) {
        $options[t('Arguments')]['%' . ++$count] = t('@argument title', array('@argument' => $handler->ui_name()));
        $options[t('Arguments')]['!' . $count] = t('@argument input', array('@argument' => $handler->ui_name()));
      }

      if (!empty($options)) {
        $output = '<p>' . t('The following tokens are available. If you would like to have the characters \'[\' and \']\' please use the html entity codes \'%5B\' or  \'%5D\' or they will get replaced with empty space.' . '</p>');
        foreach (array_keys($options) as $type) {
          if (!empty($options[$type])) {
            $items = array();
            foreach ($options[$type] as $key => $value) {
              $items[] = $key . ' == ' . check_plain($value);
            }
            $output .= theme('item_list',
              array(
                'items' => $items,
                'type' => $type
              ));
          }
        }

        $form['token_help'] = array(
          '#type' => 'fieldset',
          '#title' => t('Replacement patterns'),
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#states' => array(
            'visible' => array(
              ':input[name="options[tokenize]"]' => array('checked' => TRUE),
            ),
          ),
          '#value' => $output,
          '#id' => 'edit-options-token-help',
          '#prefix' => '<div>',
          '#suffix' => '</div>',
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function options_submit(&$form, &$form_state) {
    if (!empty($form_state['values']['options']['then'])) {
      $form_state['values']['options']['then_format'] = $form_state['values']['options']['then']['format'];
      $form_state['values']['options']['then'] = $form_state['values']['options']['then']['value'];
    }
    if (!empty($form_state['values']['options']['else'])) {
      $form_state['values']['options']['else_format'] = $form_state['values']['options']['else']['format'];
      $form_state['values']['options']['else'] = $form_state['values']['options']['else']['value'];
    }

    parent::options_submit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  function render($empty = FALSE) {
    $handler = !empty($this->options['if']) ? $this->getFilters($this->options['if']) : '';

    // If no filter handler is available then just return some empty text as
    // something has gone wrong.
    if (empty($handler)) {
      return '';
    }

    // Build the validation callback. This is what determines which text area
    // is rendered at the end.
    $operators = $this->operators($handler);
    $callback = $operators[$this->options['condition']]['validate'] . 'Validate';

    if ($this->{$callback}($handler)) {
      $content = $this->options['then'];
      $format = isset($this->options['then_format']) ? $this->options['then_format'] : filter_default_format();
    }
    else {
      $content = $this->options['else'];
      $format = isset($this->options['else_format']) ? $this->options['else_format'] : filter_default_format();
    }

    if (!$empty || !empty($this->options['empty'])) {
      return $this->render_textarea($content, $format);
    }

    return '';
  }

  /**
   * Render a text area, using the proper format.
   */
  public function render_textarea($value, $format) {
    if ($value) {
      if ($this->options['tokenize']) {
        $value = $this->view->style_plugin->tokenize_value($value, 0);
      }
      return check_markup($value, $format, '', FALSE);
    }
  }

  /**
   * Helper method to return all exposed filters for the view. If $option is set
   * then that specific handler will be returned if it exists.
   *
   * @var string $option
   *   The name of an exposed filter.
   *
   * @return views_handler_filter|bool|array
   *   A views_handler_filter if $option has been set and it exists otherwise
   *   FALSE. An array of all views_handler_filter objects attached to the view
   *   if $option has not been passed in.
   */
  function getFilters($option = '') {
    $filters = $this->view->display_handler->get_handlers('filter');

    if (!empty($option)) {
      return isset($filters[$option]) ? $filters[$option] : FALSE;
    }

    return $filters;
  }

  /**
   * If the filter has selected options then radios are shown to allow a user to
   * select a specific value. If it doesn't then a textfield is shown to allow
   * the user to input a conditional value.
   */
  public function equalsFormElement(&$form, $form_state, $handler) {
    views_conditional_area_add_equals_form_element($form, $form_state, $handler, $this->options['is']);
  }

  /**
   * Adds a textfield for the conditional value.
   */
  public function simpleTextFormElement(&$form, $form_state, $handler) {
    views_conditional_area_add_simple_text_form_element($form, $form_state, $handler, $this->options['is']);
  }

  /**
   * Returns checkboxes for the element with the options available for the filer
   * allowing users to select the values to be compared against.
   */
  public function inFormElement(&$form, $form_state, $handler) {
    views_conditional_area_add_in_form_element($form, $form_state, $handler, $this->options['checkbox_is']);
  }

  /**
   * Validates a "equals" exposed filter to confirm which conditional area
   * should be displayed.
   *
   * @var views_handler_filter $handler
   *   The exposed filter of which the condition is based on.
   *
   * @return bool
   */
  public function equalsValidate($handler) {
    return views_conditional_area_validate_equals($handler, $this->options);
  }

  /**
   * Validates a "not equals" exposed filter to confirm which conditional area
   * should be displayed.
   *
   * @var views_handler_filter $handler
   *   The exposed filter of which the condition is based on.
   *
   * @return bool
   */
  public function notEqualsValidate($handler) {
    return !$this->equalsValidate($handler);
  }

  /**
   * Validates a "in" exposed filter to confirm which conditional area
   * should be displayed.
   *
   * @var views_handler_filter $handler
   *   The exposed filter of which the condition is based on.
   *
   * @return bool
   */
  public function inValidate($handler) {
    return views_conditional_area_validate_in($handler, $this->options);
  }

  /**
   * Validates a "not in" exposed filter to confirm which conditional area
   * should be displayed.
   *
   * @var views_handler_filter $handler
   *   The exposed filter of which the condition is based on.
   *
   * @return bool
   */
  public function notInValidate($handler) {
    return !$this->inValidate($handler);
  }

  /**
   * Validates a "contains" exposed filter to confirm which conditional area
   * should be displayed.
   *
   * @var views_handler_filter $handler
   *   The exposed filter of which the condition is based on.
   *
   * @return bool
   */
  public function containsValidate($handler) {
    return views_conditional_area_validate_contains($handler, $this->options);
  }

  /**
   * Validates a "not contains" exposed filter to confirm which conditional area
   * should be displayed.
   *
   * @var views_handler_filter $handler
   *   The exposed filter of which the condition is based on.
   *
   * @return bool
   */
  public function notContainsValidate($handler) {
    return !$this->containsValidate($handler);
  }

  /**
   * Validates a "empty" exposed filter to confirm which conditional area
   * should be displayed.
   *
   * @var views_handler_filter $handler
   *   The exposed filter of which the condition is based on.
   *
   * @return bool
   */
  public function emptyValidate($handler) {
    return views_conditional_area_validate_empty($handler);
  }

  /**
   * Validates a "not empty" exposed filter to confirm which conditional area
   * should be displayed.
   *
   * @var views_handler_filter $handler
   *   The exposed filter of which the condition is based on.
   *
   * @return bool
   */
  public function notEmptyValidate($handler) {
    return !$this->emptyValidate($handler);
  }

}