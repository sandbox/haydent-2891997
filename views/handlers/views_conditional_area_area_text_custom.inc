<?php

/**
 * @file
 *  Defines the views_conditional_area_area_text_custom handler.
 */

class views_conditional_area_text_custom extends views_conditional_area_test {

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    unset($options['then_format']);
    unset($options['else_format']);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Alter the form element, to be a regular text area.
    if (isset($form['then'])) {
      $form['then']['#type'] = 'textarea';
      unset($form['then']['#format']);
      unset($form['then']['#wysiwyg']);
    }
    if (isset($form['else'])) {
      $form['else']['#type'] = 'textarea';
      unset($form['else']['#format']);
      unset($form['else']['#wysiwyg']);
    }
  }

  /**
   * Empty, so we don't inherit options_submit from the parent.
   */
  public function options_submit(&$form, &$form_state) {
  }

  /**
   * {@inheritdoc}
   */
  function render($empty = FALSE) {
    $handler = !empty($this->options['if']) ? $this->getFilters($this->options['if']) : '';

    // If no filter handler is available then just return some empty text as
    // something has gone wrong.
    if (empty($handler)) {
      return '';
    }

    // Build the validation callback. This is what determines which text area
    // is rendered at the end.
    $operators = $this->operators($handler);
    $callback = $operators[$this->options['condition']]['validate'] . 'Validate';

    if ($this->{$callback}($handler)) {
      $content = $this->options['then'];
    }
    else {
      $content = $this->options['else'];
    }

    if (!$empty || !empty($this->options['empty'])) {
      return $this->render_textarea_custom($content);
    }

    return '';
  }

  /**
   * Render a text area with filter_xss_admin.
   */
  public function render_textarea_custom($value) {
    if ($value) {
      if ($this->options['tokenize']) {
        $value = $this->view->style_plugin->tokenize_value($value, 0);
      }
      return $this->sanitize_value($value, 'xss_admin');
    }
  }

}