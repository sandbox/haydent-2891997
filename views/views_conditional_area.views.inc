<?php

/**
 * @file
 * Provide views data and handlers.
 */

/**
 * Implements hook_views_data().
 */
function views_conditional_area_views_data() {
  $data = array();

  // Conditional text area.
  $data['views']['views_conditional_area_text'] = array(
    'title' => t('Conditional text area'),
    'help' => t('Provide conditional markup text for the area based on a field value.'),
    'area' => array(
      'handler' => 'views_conditional_area_text',
    ),
  );

  // Conditional unfiltered text.
  $data['views']['views_conditional_area_text_custom'] = array(
    'title' => t('Conditional unfiltered text'),
    'help' => t('Add conditional unrestricted, custom text or markup based on a field value. This is similar to the custom text field.'),
    'area' => array(
      'handler' => 'views_conditional_area_text_custom',
    ),
  );

  // Conditional view.
  $data['views']['views_conditional_area_view'] = array(
    'title' => t('Conditional view area'),
    'help' => t('Insert a view into the area based on a field value.'),
    'area' => array(
      'handler' => 'views_conditional_area_view',
    ),
  );

  return $data;
}